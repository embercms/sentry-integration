<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'bf5737dae747359635ef25452dad77f55bfe6112',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'bf5737dae747359635ef25452dad77f55bfe6112',
            'dev_requirement' => false,
        ),
        'clue/stream-filter' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clue/stream-filter',
            'aliases' => array(),
            'reference' => 'aeb7d8ea49c7963d3b581378955dbf5bc49aa320',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'http-interop/http-factory-guzzle' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../http-interop/http-factory-guzzle',
            'aliases' => array(),
            'reference' => '8f06e92b95405216b237521cc64c804dd44c4a81',
            'dev_requirement' => false,
        ),
        'jean85/pretty-package-versions' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jean85/pretty-package-versions',
            'aliases' => array(),
            'reference' => 'ae547e455a3d8babd07b96966b17d7fd21d9c6af',
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-common' => array(
            'pretty_version' => '2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/client-common',
            'aliases' => array(),
            'reference' => 'd135751167d57e27c74de674d6a30cef2dc8e054',
            'dev_requirement' => false,
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/discovery' => array(
            'pretty_version' => '1.14.1',
            'version' => '1.14.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/discovery',
            'aliases' => array(),
            'reference' => 'de90ab2b41d7d61609f504e031339776bc8c7223',
            'dev_requirement' => false,
        ),
        'php-http/httplug' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/httplug',
            'aliases' => array(),
            'reference' => '191a0a1b41ed026b717421931f8d3bd2514ffbf9',
            'dev_requirement' => false,
        ),
        'php-http/message' => array(
            'pretty_version' => '1.12.0',
            'version' => '1.12.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message',
            'aliases' => array(),
            'reference' => '39eb7548be982a81085fe5a6e2a44268cd586291',
            'dev_requirement' => false,
        ),
        'php-http/message-factory' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message-factory',
            'aliases' => array(),
            'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
            'dev_requirement' => false,
        ),
        'php-http/message-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'php-http/promise' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/promise',
            'aliases' => array(),
            'reference' => '4c4c1f9b7289a2ec57cde7f1e9762a5789506f88',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '^1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'psr/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'sentry/sdk' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '2de7de3233293f80d1e244bd950adb2121a3731c',
            'dev_requirement' => false,
        ),
        'sentry/sentry' => array(
            'pretty_version' => '3.3.4',
            'version' => '3.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sentry/sentry',
            'aliases' => array(),
            'reference' => 'ecbd09ea5d053a202cf773cb24ab28af820831bd',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => 'c726b64c1ccfe2896cb7df2e1331c357ad1c8ced',
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v6.0.0',
            'version' => '6.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'reference' => '39f34cd5d28cd263b95a58ebad18421b6fefc4ba',
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'reference' => '265f03fed057044a8e4dc159aa33596d0f48ed3f',
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '3.0',
            ),
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v6.0.0',
            'version' => '6.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'reference' => 'be0facf48a42a232d6c0daadd76e4eb5657a4798',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-uuid' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-uuid',
            'aliases' => array(),
            'reference' => '9165effa2eb8a31bb3fa608df9d529920d21ddd9',
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => '36715ebf9fb9db73db0cb24263c79077c6fe8603',
            'dev_requirement' => false,
        ),
    ),
);
